CREATE OR REPLACE FUNCTION jacard(arg1 text, arg2 text, qgram integer) RETURNS DOUBLE PRECISION AS $$
DECLARE
qgramOuter text;
qgramInner text;
qgramNum1 double precision;
qgramNum2 double precision;
match integer;
jacardRes double precision;

BEGIN
match = 0;

FOR i IN 1..char_length(arg1)-qgram+1 LOOP
	qgramOuter = substring(arg1 from i for qgram);
   
	FOR j in 1..char_length(arg2)-qgram+1 LOOP
		qgramInner = substring(arg2 from j for qgram);
		IF (qgramOuter = qgramInner) THEN
			match = match+1;
			EXIT;
		END IF;
 
	END LOOP;
END LOOP;

qgramNum1 = char_length(arg1)-qgram+1;
qgramNum2 = char_length(arg2)-qgram+1;
 
jacardRes = match/(qgramNum1+qgramNum2-match);

RETURN jacardRes;
END;
$$ LANGUAGE plpgsql;